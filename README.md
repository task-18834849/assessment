# assessment


Section A : Making a simple website with JS

Section A : Making a simple website with JS
Task 1 - BootStrap
a. Use this page, and create a modal - which gets called - whenever the user clicks on the pricing. The modal should contain a form - asking following details:

  Name

  Email

  Order Comments
  
From the bootstrap template, one should be able to click on any of the buttons of the three pricing tables, and fill in a form.


b. Use a slider - which should allow user to scroll between the number of users - and suppose if the number of users are 0-10, then first plan should get highlighted, 10-20, the second plan should get highlighted, and so on.
Task 2 - JS

Sign up on https://forms.maakeetoo.com

from task 1 - the contents of form submission - should be populated there.

   steps 
 * create a folder on desktop with a name of your choice
 * open that folder in vs code.
 * create a file inside that folder with index.html name.
 * write your code in this folder for your project.
 * from file option present in title bar choose auto save option so whatever changes you are 
   doing in your code will get save automatically.
 * i have written all my code in index.html file only.
 * you can create a seperate file for css and js with extension of style.css and script.js
 * in this i have used internal css but using external css is better because it makes our code 
   look clean and simple.
 * for that you will write code in style.css file and give its link in index.html code in head 
   tag to make it work
 * for js you write your code in script.js will give its link in index.html code in body tag.
 * first  we will write html code then js code and after that css code.
 * according to our given class ids in html code we will design our code using css.
 * we can choose text-style,margin, text-color,background-color,alignment etc. 
 * js we will write main working and function because it does work like a brain of a code.
 * html is for outer body and css is for designing.
 * in this code i have used container which contains order now button,carousel/slider.
 * after that form which contains text-box for data input like name,email,order comment. 
 * it also contains cancel and order now button for form witha title  bar of place you order.
 * if all details is note filled msg will pop out to fill all details.
 * if all details are filled  click on order now button it will show order placed successfully.
 * if using same credentials  clicking on order now button it will show order already exist.
 * now you can deploy your code on netlify or any other site.
 * just drag and drop your index.html file in netlify.
 * now share all details in readme md file like deploy link,screenshot of output, output video 
   etc.

 ## screen shot

![Alt text](<Screenshot (4).png>)

![Alt text](<Screenshot (5).png>)

![Alt text](image.png)

![Alt text](image-1.png)

![Alt text](image-2.png)

![Alt text](image-3.png)

![Alt text](image-4.png)

 ### video output 

<video src="Bootstrap%20Modal%20and%20Slider%20-%20Personal%20-%20Microsoft%E2%80%8B%20Edge%202023-12-07%2023-51-03.mp4" controls title="Title"></video>


 ### deploy link

https://65721b62d7dec20090f185ca--fantastic-donut-aaf613.netlify.app/


